var btn = document.querySelector('.burger')
var menu = document.querySelector('.header-menu')

btn.addEventListener('click', function(){
    menu.classList.toggle('header-menuOpen')
    btn.classList.toggle('toggle')})



//lien actif
    //div container
    var lienContainer = document.getElementById("menu");

    //sélectionner les liens présents dans le container
    var liens = lienContainer.getElementsByClassName("lien");

    // ajoute "actif" au lien actuellement cliqué
    for (var i = 0; i < liens.length; i++) {
        liens[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("actif");
        current[0].className = current[0].className.replace(" actif", "");
    this.className += " actif";
  });
}

//Horloge
function showTime(){
  var date = new Date();
  var h = date.getHours(); // 0 - 23
  var m = date.getMinutes(); // 0 - 59
  var s = date.getSeconds(); // 0 - 59
   
  h = (h < 10) ? "0" + h : h;
  m = (m < 10) ? "0" + m : m;
  s = (s < 10) ? "0" + s : s;
  
  var time = h + ":" + m;
  if (m%2 == 0){
    document.getElementById("monhorloge").innerHTML = '<div style = "color: white;">'+ time + '</div>';
  }
  else{
    document.getElementById("monhorloge").innerHTML = '<div style = "color: white;">'+ time + '</div>';  
  }
  setInterval(showTime, 1000);
}
showTime();


/* cacher le bouton UP */
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("up").style.bottom = "30px";
  } else {
    document.getElementById("up").style.bottom = "-100px";
  }
  prevScrollpos = currentScrollPos;
}

//Diaporama
var slideIndex = 0;

// flèches directionnelles
function plusSlides(n) {
showSlides(slideIndex += n);
}

//images
showSlides(slideIndex);    
function showSlides(n) {
  var i;
    var slides = document.getElementsByClassName("mySlides1");
    if(slides.length <= 0) return;
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1}
    slides[slideIndex-1].style.display = "block";
    setTimeout(showSlides, 3000);
}


//Sticky navbar

// When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};
// Get the navbar
var navbar = document.getElementById("menu");

// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
} 